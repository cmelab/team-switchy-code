#!/usr/bin/env sh

rsync -rav --ignore-existing --progress rachelsingleton@fry.boisestate.edu:/home/rachelsingleton/three-particle-switchy/pythonfiles/transfer_jobs/* three_trap_epsilon_50/workspace/
