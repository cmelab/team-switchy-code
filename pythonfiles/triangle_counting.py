# This code takes in a simulation, counts the triangles that are created, and 
# displays the data in a table using pandas!
# Make sure to correctly specify the root of where our workspace is!

# Importing important things for our code to work correctly!
import hoomd
import hoomd.md
import json
import numpy as np
import ex_render
import matplotlib.pyplot as plt
import freud
from IPython.display import Image
import os.path
from prettytable import PrettyTable
import gsd
import gsd.pygsd
import pickle
from uncertainties import ufloat
import uncertainties.umath as umath
from trap_def import trap_types
import tri2_analysis as tri
from trap_cutoff import cutoffs
import os
import pandas as pd

hoomd.context.initialize("");

from flow import FlowProject
root = "../higher_temps_epsilon_50" # Will need to change this if you run this code from somewhere else
#proj = FlowProject.get_project() # used when the project is in the same folder as this file
proj = FlowProject.get_project(root=root)
appendedList = []
for job in proj.find_jobs():
    trap_type = job.sp["trap_type"]
    gsdfile = job.fn("trajectory.gsd")
    snapshot = hoomd.data.gsd_snapshot(gsdfile,frame=-1)
    num = cutoffs[trap_type]
    data = [trap_type,job.sp["kT"],tri.count_triangles(snapshot,d_cluster=num),job.sp["mix_kT"],job.sp["epsilon"]]
    appendedList.append(data)
df = pd.DataFrame(appendedList, columns = ['Trapezoid Type','kT','Number of Triangles','mix_kT','epsilon'])

# In the command line, you can send this output to a file but once you do that, it just exists and you can't change it. 
# If you want to do sorting or find a specific value, it needs to be done in here where you can access the DataFrame.
df = df.sort_values(by=['Number of Triangles'],ascending=False)
df.to_csv('triangles/good_triangle_data', index=False)
