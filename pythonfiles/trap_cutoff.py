# This includes the d_cluster for each specific trapezoid that we run simulations with. 
# Since each one creates a slightly different triangle and different defects, they can't
# all use the same d_cluster. This will be another parameter that we will pass into the 
# triangle_counting function which is in tri2_analysis.py.

cutoffs={}

cutoffs["Alpha"]=1.9
cutoffs["Beta"]=1.9
cutoffs["Delta"]=1.8
cutoffs["Eta"]=1.6
cutoffs["Gamma"]=2
cutoffs["Iota"]=1.7
cutoffs["Kappa"]=1.7
cutoffs["Lamda"]=1.6
cutoffs["Omicron"]=2
cutoffs["Zeta"]=2
cutoffs[('Lamda1', 'Lamda2', 'Lamda3')]=1.6
