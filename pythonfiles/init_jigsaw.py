import signac


def main():
    project = signac.init_project("Jigsaw-Signac")
    for kT in [0.2, 0.5, 1, 2, 5, 10]:
        for trap_type in [
            "Alpha",
            "Beta",
            "Gamma",
            #("Gamma1","Gamma2","Gamma3")
            "Delta",
            "Zeta",
            "Eta",
            "Iota",
            "Kappa",
            "Lamda",
            "Omicron",
        ]:
            statepoint = {"kT": kT}
            statepoint["sticky"] = True
            statepoint["epsilon"] = 50
            statepoint["mix_kT"] = 50
            statepoint["trap_type"] = trap_type
            job = project.open_job(statepoint)
            job.init()


if __name__ == "__main__":
    main()
