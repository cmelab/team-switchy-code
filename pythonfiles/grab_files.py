import signac

proj = signac.get_project()
filters = {'epsilon': 50}
proj.find_jobs(filters).export_to('transfer_jobs', path=False)
